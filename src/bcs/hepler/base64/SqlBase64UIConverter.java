package bcs.hepler.base64;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class SqlBase64UIConverter {

    private static final int D = 12;
    private static final int T = 400;
    private static final int B = 130;
    private static final int E = 500;
    private static final int N = 23;

    private final JFrame frame;
    private final JTextArea textSql;
    private final JTextArea textBase64;
    private final JTextArea base64Map;

    private final static String BASE64_DEFAULT_MAP = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    public static void main(String[] args) {
        SqlBase64UIConverter main = new SqlBase64UIConverter();
        System.out.println(main.generateBase64Map());
        System.out.println(main.generateBase64Map());
        System.out.println(main.generateBase64Map());
        System.out.println(main.generateBase64Map());
        System.out.println(main.generateBase64Map());
        System.out.println(main.generateBase64Map());
        main.frame.setVisible(true);
    }


    private SqlBase64UIConverter() {
        frame = new JFrame("Base64 Coder");
        frame.setLayout(null);
        frame.setBounds(150, 150, 4*D+2*T+B, N+3*D+E);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        JButton useDefaultMap = new JButton("standart");
        useDefaultMap.setBounds(D, D, B, N);
        JButton generateBase64Map = new JButton("generate");
        generateBase64Map.setBounds(2*D+B, D, B, N);
        JButton sql2base64 = new JButton("sql -> base64");
        JButton base642sql = new JButton("sql <- base64");
        base64Map = new JTextArea();
        base64Map.setText(BASE64_DEFAULT_MAP);
        base64Map.setBounds(3*D+2*B, D, (4*D+2*T+B) - (3*D+2*B) - D, N);
        textSql = new JTextArea();
        textSql.setText("<Enter sql text end press '" + sql2base64.getText() + "'>");
        textBase64 = new JTextArea();
        textBase64.setText("<Enter base64 text end press '" + base642sql.getText() + "'>");
        sql2base64.setBounds(2*D+T, N+2*D, B, N);
        base642sql.setBounds(2*D+T, N+3*D+N, B, N);
        textSql.setLineWrap(true);
        textBase64.setLineWrap(true);
        textSql.setBounds(D, N+2*D, T, E);
        textBase64.setBounds(D*3 +T + B, N+2*D, T, E);
        textSql.setAutoscrolls(true);
        textBase64.setAutoscrolls(true);

        sql2base64.addActionListener(new SqlToBase64ActionListener());
        base642sql.addActionListener(new Base64ToSqlActionListener());
        generateBase64Map.addActionListener(new GenerateActionListener());
        useDefaultMap.addActionListener(new DefaultActionListener());

        frame.add(sql2base64);
        frame.add(base642sql);
        frame.add(useDefaultMap);
        frame.add(generateBase64Map);
        frame.add(base64Map);
        textSql.setBounds(D, N+2*D, T, E);
        textBase64.setBounds(D*3 +T + B, N+2*D, T, E);
        frame.add(textBase64);
        frame.add(textSql);
    }

    private String generateBase64Map() {
        TreeMap<Double, Character> randomChars = new TreeMap<>();
        for (char ch : BASE64_DEFAULT_MAP.toCharArray()) {
            Double random = Math.random();
            while (randomChars.containsKey(random)) {
                random = Math.random();
            }
            randomChars.put(random, ch);
        }
        StringBuilder result = new StringBuilder();
        for (char ch : randomChars.values()) {
            result.append(ch);
        }
        return new String(result);
    }

    private String isBase64MapValid() {
        String base64MapString = base64Map.getText();
        if (base64MapString.length() != BASE64_DEFAULT_MAP.length()) {
            return "base64MapString.length() != BASE64_DEFAULT_MAP.length()";
        }
        Set<String> tempSet = new HashSet<>();
        for (Character ch : base64MapString.toCharArray()) {
            tempSet.add(String.valueOf(ch));
        }
        if (tempSet.size() != BASE64_DEFAULT_MAP.length()) {
            return "tempSet.size() != BASE64_DEFAULT_MAP.length()";
        }
        return null;
    }

    private class Base64ToSqlActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String errList = isBase64MapValid();
            if (errList != null) {
                textSql.setText(errList);
                return;
            }
            String base64Text = textBase64.getText();
            Map<String, String> map = new HashMap<>();
            String base64MapString = base64Map.getText();
            for (int i=0; i<BASE64_DEFAULT_MAP.length(); i++) {
                map.put(base64MapString.substring(i, i+1),
                        BASE64_DEFAULT_MAP.substring(i, i+1));
            }
            StringBuilder result = new StringBuilder();
            for (Character ch : base64Text.toCharArray()) {
                String key = String.valueOf(ch);
                result.append(map.getOrDefault(key, key));
            }
            try {
                textSql.setText(new String(Base64.getUrlDecoder().decode(new String(result))));
            } catch (Exception err) {
                textSql.setText(UUID.randomUUID().toString() + " " + err.getClass().getName() + " " + err.getMessage());
            }
        }

    }

    private class SqlToBase64ActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String errList = isBase64MapValid();
            if (errList != null) {
                textBase64.setText(errList);
                return;
            }
            String sqlString = textSql.getText();
            String base64String = new String(Base64.getUrlEncoder().encode(sqlString.getBytes()));
            Map<String, String> map = new HashMap<>();
            String base64MapString = base64Map.getText();
            for (int i=0; i<BASE64_DEFAULT_MAP.length(); i++) {
                map.put(BASE64_DEFAULT_MAP.substring(i, i+1),
                        base64MapString.substring(i, i+1));
            }
            StringBuilder result = new StringBuilder();
            for (Character ch : base64String.toCharArray()) {
                String key = String.valueOf(ch);
                result.append(map.getOrDefault(key, key));
            }
            textBase64.setText(new String(result));
        }
    }
    private class GenerateActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            base64Map.setText(generateBase64Map());
        }
    }
    private class DefaultActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            base64Map.setText(BASE64_DEFAULT_MAP);
        }
    }

}
