package ui.examples.hypercube.utils;

import ui.examples.hypercube.Point2D;
import ui.examples.hypercube.PointND;

import java.util.ArrayList;

import static ui.examples.hypercube.consts.HyperCubeSettings.*;

public class NCubeUtils {

    private static Point2D pointND2Point2DPerspective(PointND pointND) {
        Double x = 0D, y = 0D;
        ArrayList<Double> cords = pointND.getX();
        int cordCount = pointND.getN();
        if (cordCount > 0) {
            x = cords.get(0);
        }
        if (cordCount > 1) {
            y = cords.get(1);
        }
        if (USE_PERSPECTIVE) {
            if (cordCount > 2) {
                double k = (cords.get(2) + Z0) / ZZ;
                x *= k;
                y *= k;
            }
        }
        return new Point2D(x, y);
    }

    public static Point2D pointND2Point2D(PointND pointND) {
        if (!USE_ALTERNATIVE) {
            return pointND2Point2DPerspective(pointND);
        }
        Double x = 0D, y = 0D;
        ArrayList<Double> cords = pointND.getX();
        int cordCount = pointND.getN();
        for (int i = 0; i < cordCount; i++) {
            Double angle = i * Math.PI / cordCount;
            x += cords.get(i) * Math.cos(angle);
            y += cords.get(i) * Math.sin(angle);
        }
        return new Point2D(x, y);
    }

    private NCubeUtils() {}
}
