package ui.examples.hypercube;

public class Line {

    private PointND p1, p2;

    public Line(PointND p1, PointND p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public PointND getP1() {
        return p1;
    }

    public PointND getP2() {
        return p2;
    }
}
