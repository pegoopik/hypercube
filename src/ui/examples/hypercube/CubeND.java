package ui.examples.hypercube;

import ui.examples.hypercube.consts.HyperCubeSettings;

import java.util.ArrayList;

public class CubeND {

    private final int n;
    private final int pointCount;
    private final double sideSize;
    private ArrayList<PointND> defaultPoints;
    private ArrayList<PointND> points;
    private ArrayList<Line> lines;
    private ArrayList<Double> angles;
    private final int angleCount;

    public CubeND(int n) {
        this(n, HyperCubeSettings.SIDE_SIZE);
    }

    public CubeND(int n, double sideSize) {
        this.n = n;
        this.sideSize = sideSize;
        this.pointCount = (int)Math.round(Math.pow(2, n));
        defaultPoints = new ArrayList();
        points = new ArrayList();
        angleCount = n*(n-1)/2;
        lines = new ArrayList<>();
        angles = new ArrayList<>();
        for (int i = 0; i < angleCount; i++) {
            angles.add(0D);
        }

        for (int i = 0; i < pointCount; i++) {
            ArrayList<Double> defaultX = new ArrayList<>();
            ArrayList<Double> x = new ArrayList<>();
            int currP = 1;
            for (int j = 0; j < n; j++) {
                defaultX.add(sideSize * (i / currP % 2 == 0 ? -0.5 : 0.5));
                x.add(sideSize * (i / currP % 2 == 0 ? -0.5 : 0.5));
                currP *= 2;
            }
            defaultPoints.add(new PointND(defaultX));
            points.add(new PointND(x));
        }
        rotate();
        for (int i = 0; i < pointCount - 1; i++) {
            for (int j = i + 1; j < pointCount; j++) {
                double r = 0;
                for (int k = 0; k < n; k++) {
                    double dX = points.get(i).getX().get(k) - points.get(j).getX().get(k);
                    r += dX * dX;
                }
                if (r <= 1.01 * sideSize * sideSize) {
                    lines.add(new Line(points.get(i), points.get(j)));
                }
            }
        }
    }

    @Override
    public String toString() {
        String res = "pointCount = " + pointCount + ", lineCount = " + lines.size();
        for (int i = 0; i < pointCount; i++) {
            res += "\n";
            for (int j = 0; j < n; j++) {
                res += " " + defaultPoints.get(i).getX().get(j);
            }
        }
        return res;
    }

    public ArrayList<Line> getLines() {
        return lines;
    }

    public ArrayList<PointND> getPoints() {
        return points;
    }

    public void move(Double[] dAngle) {
        for (int i = 0; i < dAngle.length; i++) {
            if (angleCount > i) {
                Double angle = angles.get(i);
                angle += dAngle[i];
                //normalize
                angle -= angle >= 2 * Math.PI ? 2 * Math.PI : 0;
                angle += angle <= -2 * Math.PI ? 2 * Math.PI : 0;
                angles.set(i, angle);
            }
        }
        synchronized(this) {
            rotate();
        }
    }

    private void rotate() {
        //обнулим текущий поворот
        for (int i = 0; i < pointCount; i++) {
            for (int j = 0; j < n; j++) {
                points.get(i).getX().set(j, defaultPoints.get(i).getX().get(j));
            }
        }
        //счетчик номера угла
        int k = 0;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i+1; j < n; j++) {
                Double angle = angles.get(k);
                for (PointND point : points) {
                    Double x1 = point.getX().get(i) * Math.cos(angle) + point.getX().get(j) * Math.sin(angle);
                    Double x2 = - point.getX().get(i) * Math.sin(angle) + point.getX().get(j) * Math.cos(angle);
                    point.getX().set(i, x1);
                    point.getX().set(j, x2);
                }
                k++;
            }
        }
    }


}
