package ui.examples.hypercube;

import java.util.ArrayList;
import java.util.List;

public class PointND {

    private final int n;
    private ArrayList<Double> x;

    public PointND(int n) {
        this.n = n;
        this.x = new ArrayList<>();
    }

    public PointND(ArrayList<Double> x) {
        this.n = x.size();
        this.x = x;
    }

    public ArrayList<Double> getX() {
        return x;
    }

    public int getN() {
        return n;
    }
}
