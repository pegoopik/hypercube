package ui.examples.hypercube.ui;

import ui.examples.hypercube.CubeND;
import ui.examples.hypercube.Line;
import ui.examples.hypercube.Point2D;
import ui.examples.hypercube.PointND;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;

import static ui.examples.hypercube.consts.HyperCubeSettings.*;
import static ui.examples.hypercube.utils.NCubeUtils.pointND2Point2D;

public class CubeNDPanel extends JPanel {

    private final CubeND cubeND;
    private final int cubeCenterX, cubeCenterY;

    public CubeNDPanel(CubeND cubeND, int cubeCenterX, int cubeCenterY) {
        this.cubeND = cubeND;
        this.cubeCenterX = cubeCenterX;
        this.cubeCenterY = cubeCenterY;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        synchronized (cubeND) {
            g2.setColor(LINE_COLOR);
            for (Line line : cubeND.getLines()) {
                Point2D p1 = pointND2Point2D(line.getP1());
                Point2D p2 = pointND2Point2D(line.getP2());
                Line2D.Double line2D = new Line2D.Double(
                        p1.getX() + cubeCenterX, p1.getY() + cubeCenterY, p2.getX() + cubeCenterX, p2.getY() + cubeCenterY);
                g2.draw(line2D);
            }
            g2.setColor(ONE_POINT_COLOR);
            for (PointND point : cubeND.getPoints()) {
                Point2D point2D = pointND2Point2D(point);
                Ellipse2D.Double ellipse2D = new Ellipse2D.Double(
                        point2D.getX() - 3 + cubeCenterX, point2D.getY() - 3 + cubeCenterY, 6, 6);
                g2.draw(ellipse2D);
                g2.setColor(POINT_COLOR);
            }
        }
    }

}
