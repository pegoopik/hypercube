package ui.examples.hypercube.consts;

import java.awt.*;

public class HyperCubeSettings {

    public static final int CUBE_N = 3; //размерность куба
    public static final double SIDE_SIZE = 90; //сторона куба
    //координаты начала координат на экране
    public static final int CENTER_X = 300;
    public static final int CENTER_Y = 300;
    //размеры экрана
    public static final int WINDOW_X = 700;
    public static final int WINDOW_Y = 700;
    public static final double Z0 = 500; //расстояние от зрителя до центра координат
    public static final double ZZ = 470; //расстояние от зрителя до экрана
    public static final boolean USE_PERSPECTIVE = true; // использовать перспективу (сначала в 3D, потом в 2D режим)
    // использовать альтернативное (симметричное проецирование сразу на 2D) Перспектива тут не работает
    public static final boolean USE_ALTERNATIVE = false;

    public static final Color LINE_COLOR = Color.LIGHT_GRAY;
    public static final Color POINT_COLOR = Color.BLUE;
    public static final Color ONE_POINT_COLOR = Color.RED;


    /**
     * Углы поворота (за одну итерацию)
     * "0 * " в начале - отключение вращение вокруг данного пространства вращения
     */
    public static final Double[] ANGLE_SPEEDS = new Double[] {
            1 * Math.PI / 140,
            1 * Math.PI / 70,
            1 * Math.PI / 70,
            1 * Math.PI / 220,
            1 * Math.PI / 710,
            1 * Math.PI / 720,
            1 * Math.PI / 730,
            1 * Math.PI / 740
    };

    private HyperCubeSettings() {}
}
