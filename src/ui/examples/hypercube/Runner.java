package ui.examples.hypercube;

import ui.examples.hypercube.ui.CubeNDFrame;
import ui.examples.hypercube.ui.CubeNDPanel;

import javax.swing.*;

import static ui.examples.hypercube.consts.HyperCubeSettings.*;

public class Runner {

    public static void main(String[] args) {
        CubeND cube = new CubeND(CUBE_N);
        System.out.print(cube);
        CubeNDPanel cubeNDPanel = new CubeNDPanel(cube, CENTER_X, CENTER_Y);
        CubeNDFrame cubeNDFrame = new CubeNDFrame(cubeNDPanel, WINDOW_X, WINDOW_Y);
        cubeNDFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        cubeNDFrame.show();
        for (;;) {
            cube.move(ANGLE_SPEEDS);
            cubeNDFrame.repaint();
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {}
        }
    }

}
