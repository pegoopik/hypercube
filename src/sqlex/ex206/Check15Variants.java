package sqlex.ex206;

import java.util.HashSet;
import java.util.Set;

import static java.lang.Math.PI;

public class Check15Variants {

    private static final boolean DEBUG = false;

    private static final int TASK_COUNT = 15;
    private static final int FROM_VALUE = 1;
    private static final int TO_VALUE = 30;

    private static int a,b,c,d,e,f;
    private static final Case checkCase = new Case();

    private static Set<String> results = new HashSet<>();

    public static void main(String[] args) {
        int i=0;
        for (a = FROM_VALUE; a<TO_VALUE-4; a++)
            for (b=a+1; b<TO_VALUE-3; b++)
                for (c=b+1; c<TO_VALUE-2; c++)
                    for (d=c+1; d<TO_VALUE-1; d++)
                        for (e=d+1; e<TO_VALUE; e++)
                            for (f=e+1; f<TO_VALUE+1; f++)
                                cycle(++i);
        System.out.println(results);
    }

    private static void cycle(int n) {
        checkCase.init(a,b,c,d,e,f);
        StringBuilder sb = new StringBuilder();//Если будете считать 1..255 - этот new надо искоренить, а то память потечет
        int ci = 0;
        for(int i=0;i<TASK_COUNT;i++) {
            int k = checkCase.tasks[i].check();
            sb.append(k);
            ci+=k;
        }
        if (ci==1) {
            if (DEBUG) {
                System.out.print(String.format("%d:\t%d\t%d\t%d\t%d\t%d\t%d",n,a,b,c,d,e,f));
                System.out.println("\tS: " + sb.toString());
            }
            if (!results.contains(sb.toString())) {
                System.out.println(sb.toString() + "\t" + String.format("%d:\t%d\t%d\t%d\t%d\t%d\t%d",n,a,b,c,d,e,f));
            }
            results.add(sb.toString());
        }
    }

    private static class Case {
        Task[] tasks;
        Case(){
            tasks = new Task[TASK_COUNT];
            for(int i=0; i<TASK_COUNT; i++) {
                tasks[i] = new Task();
            }
        }
        void init(int a,int b,int c,int d,int e,int f){
            tasks[0].init(a,b,c,d,e,f);tasks[1].init(a,b,c,e,d,f);tasks[2].init(a,b,c,f,d,e);
            tasks[3].init(a,c,b,d,e,f);tasks[4].init(a,c,b,e,d,f);tasks[5].init(a,c,b,f,d,e);
            tasks[6].init(a,d,b,c,e,f);tasks[7].init(a,d,b,e,c,f);tasks[8].init(a,d,b,f,c,e);
            tasks[9].init(a,e,b,c,d,f);tasks[10].init(a,e,b,d,c,f);tasks[11].init(a,e,b,f,c,d);
            tasks[12].init(a,f,b,c,d,e);tasks[13].init(a,f,b,d,c,e);tasks[14].init(a,f,b,e,c,d);
        }
    }

    private static class Task {
        int a,b,c,d,e,f;
        double u1,u2,u3;
        void init(int a,int b,int c,int d,int e,int f){
            this.a=a;this.b=b;this.c=c;this.d=d;this.e=e;this.f=f;
        }
        int check(){
            if (a+e<=c||a+c<=e||e+c<=a||a+d<=f||a+f<=d||d+f<=a||
                b+d<=e||b+e<=d||d+e<=b||b+c<=f||b+f<=c||c+f<=b)return 0;
            u1=a*a+c*c-e*e; u1=Math.acos(u1/(2*a*c));
            u2=c*c+f*f-b*b; u2=Math.acos(u2/(2*c*f));
            u3=f*f+a*a-d*d; u3=Math.acos(u3/(2*f*a));
            if (u1+u2<=u3||u1+u3<=u2||u2+u3<=u1||u1+u2+u3>=2*PI)return 0;
            return 1;
        }
    }
}

/*
Заготовка под сообщение на форуме:

Написал програмку, которая проверяет все комбинации от 1 до 255 сторон и проверяет возможны ли тэтраэдры.
Выделил три комбинации из 15, которые для некоторых наборов длинн единственные дают тэтраэдр.
Это условие необходимости.
Далее проверяем достаточность. Во всех комбинациях длинн, в которых есть хотя бы один правильный тэтраэдр я проверил,
что есть хотя бы один правильный из моих 6ит найденых.
А значит нам достаточно проверить только шесть комбинации:
(a,b,c,f,d,e)
(a,c,b,f,d,e)
(a,d,b,f,c,e)
(a,f,b,c,d,e)
(a,f,b,d,c,e)
(a,f,b,e,c,d)
Вставляем в решение:
[spoiler][src]WITH X AS(SELECT DISTINCT CAST(B_VOL AS INT)X FROM utB)
--неравенство треугольника
,Base2 AS(SELECT*FROM(SELECT*FROM X A,X B,X C,X D,X E,X F
WHERE A.X<B.X and B.X<C.X and C.X<D.X and D.X<E.X and E.X<F.X
)T(a,b,c,d,e,f)
CROSS APPLY(
SELECT * FROM(VALUES
(a,b,c,f,d,e),
(a,c,b,f,d,e),
(a,d,b,f,c,e),
(a,f,b,c,d,e),
(a,f,b,d,c,e),
(a,f,b,e,c,d))T(a1,b1,c1,d1,e1,f1)
WHERE a1+e1>c1 and a1+c1>e1 and e1+c1>a1
  and a1+d1>f1 and a1+f1>d1 and d1+f1>a1
  and b1+d1>e1 and b1+e1>d1 and d1+e1>b1
  and b1+c1>f1 and b1+f1>c1 and c1+f1>b1
)CA)
--неравенство углов у одной из вершин
,Base3 AS(
SELECT *, ROW_NUMBER()OVER(PARTITION BY a ORDER BY b, c, d, e, f)N
FROM(
SELECT DISTINCT a, b, c, d, e, f
FROM Base2
  OUTER APPLY(
    SELECT
	  ACOS(CAST(a1*a1 + c1*c1 - e1*e1 AS FLOAT)/(2*a1*c1))u1,
	  ACOS(CAST(c1*c1 + f1*f1 - b1*b1 AS FLOAT)/(2*c1*f1))u2,
	  ACOS(CAST(f1*f1 + a1*a1 - d1*d1 AS FLOAT)/(2*f1*a1))u3
  )OA
WHERE u1+u2+u3<2*PI() AND u1+u2>u3 AND u1+u3>u2 AND u2+u3>u1)T)
--редактируем запись относительно предыдущей
,T AS(
SELECT Cnt, a, (n-1)%50 n2, (n-1)/50 n1, ','+STUFF(S,1,kk,'')S
FROM(
SELECT *,CONCAT(
  IIF(b=_b,'',CAST(b As VARCHAR(11))),'.',
  IIF(c=_c,'',CAST(c As VARCHAR(11))),'.',
  IIF(d=_d,'',CAST(d As VARCHAR(11))),'.',
  IIF(e=_e,'',CAST(e As VARCHAR(11))),'.',
  IIF(f=_f,'',CAST(f As VARCHAR(11))))S, CASE
    WHEN b<>_b THEN 0 WHEN c<>_c THEN 1
	WHEN d<>_d THEN 2 WHEN e<>_e THEN 3	ELSE 4 END kk
FROM(
SELECT *,COUNT(*)OVER(PARTITION BY a)Cnt
  ,LAG(b,1,-1)OVER(PARTITION BY a ORDER BY n)_b
  ,LAG(c,1,-1)OVER(PARTITION BY a ORDER BY n)_c
  ,LAG(d,1,-1)OVER(PARTITION BY a ORDER BY n)_d
  ,LAG(e,1,-1)OVER(PARTITION BY a ORDER BY n)_e
  ,LAG(f,1,-1)OVER(PARTITION BY a ORDER BY n)_f
FROM Base3
)T)T)
--двойным пивотом формируем результат
,PreRes AS(
SELECT Cnt, a, n1, CAST(CONCAT([0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],
[15],[16],[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31],[32],[33],
[34],[35],[36],[37],[38],[39],[40],[41],[42],[43],[44],[45],[46],[47],[48],[49]) AS VARCHAR(max))S
FROM T PIVOT(MAX(S) FOR n2 IN ([0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],
[15],[16],[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31],[32],[33],
[34],[35],[36],[37],[38],[39],[40],[41],[42],[43],[44],[45],[46],[47],[48],[49]))pvt)

SELECT Cnt, a, STUFF(CONCAT([0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],
[15],[16],[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31],[32],[33],
[34],[35],[36],[37],[38],[39],[40],[41],[42],[43],[44],[45],[46],[47],[48],[49]),1,1,'')S
FROM PreRes PIVOT(MAX(S) FOR n1 IN ([0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],
[15],[16],[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31],[32],[33],
[34],[35],[36],[37],[38],[39],[40],[41],[42],[43],[44],[45],[46],[47],[48],[49]))pvt[/src][/spoiler]
Execution time
Test query: 0.436
Your query: 0.453

Так же подобрал данные, на которых нужны ВСЕ 6 комбинаций:
[spoiler]<painting>
<utq>
<row>
<q_id>1</q_id>
<q_name>1</q_name>
</row>
</utq>
<utv>
<row>
<v_id>1</v_id>
<v_name>1</v_name>
<v_color>r</v_color>
</row>
</utv>
<utb>
<row>
<b_datetime>1900-01-06</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>2</b_vol>
</row>
<row>
<b_datetime>1900-01-08</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>3</b_vol>
</row>
<row>
<b_datetime>1900-01-09</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>4</b_vol>
</row>
<row>
<b_datetime>1900-01-10</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>5</b_vol>
</row>
<row>
<b_datetime>1900-01-11</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>6</b_vol>
</row>
<row>
<b_datetime>1900-01-12</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>7</b_vol>
</row>
<row>
<b_datetime>1900-01-15</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>8</b_vol>
</row>
<row>
<b_datetime>1900-01-17</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>10</b_vol>
</row>
<row>
<b_datetime>1900-01-18</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>11</b_vol>
</row>
<row>
<b_datetime>1900-01-19</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>12</b_vol>
</row>
<row>
<b_datetime>1900-01-20</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>14</b_vol>
</row>
<row>
<b_datetime>1900-01-21</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>16</b_vol>
</row>
<row>
<b_datetime>1900-01-22</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>17</b_vol>
</row>
<row>
<b_datetime>1900-01-23</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>19</b_vol>
</row>
<row>
<b_datetime>1900-01-24</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>20</b_vol>
</row>
<row>
<b_datetime>1900-01-25</b_datetime>
<b_q_id>1</b_q_id>
<b_v_id>1</b_v_id>
<b_vol>22</b_vol>
</row>
</utb>
</painting>[/spoiler]

На них убрав хотя бы 1 комбинацию получим несовпадения с тестовым.
Можешь проверить свои 5 комбинаций на этих данных!:)

 */