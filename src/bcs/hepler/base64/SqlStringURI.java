package bcs.hepler.base64;

//import javax.swing.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class SqlStringURI {

    private static final int D = 12;
    private static final int T = 400;
    private static final int B = 135;
    private static final int E = 500;
    private static final int N = 23;

    private final JFrame frame;
    private final JTextArea textSql;
    private final JTextArea textPreparedSql;
    private final JTextArea baseMap;

    private final static String BAD_CHARACTERS = "ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮйцукенгшщзхъфывапролджэячсмитьбю";

    public static void main(String[] args) {
        SqlStringURI main = new SqlStringURI();
        System.out.println(main.generateMap());
        System.out.println(main.generateMap());
        System.out.println(main.generateMap());
        System.out.println(main.generateMap());
        System.out.println(main.generateMap());
        System.out.println(main.generateMap());
        main.frame.setVisible(true);
    }


    private SqlStringURI() {
        frame = new JFrame("Prepared SQL Creator");
        frame.setLayout(null);
        frame.setBounds(150, 150, 4*D+2*T+B, N+3*D+E);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        JButton useDefaultMap = new JButton("standart");
        useDefaultMap.setBounds(D, D, B, N);
        JButton generateMap = new JButton("generate");
        generateMap.setBounds(2*D+B, D, B, N);
        JButton sql2preparedSQL = new JButton("sql -> prepSql");
        baseMap = new JTextArea();
        baseMap.setText(BAD_CHARACTERS);
        baseMap.setBounds(3*D+2*B, D, (4*D+2*T+B) - (3*D+2*B) - D, N);
        textSql = new JTextArea();
        textSql.setText("<Enter sql text end press '" + sql2preparedSQL.getText() + "'>");
        textPreparedSql = new JTextArea();
        sql2preparedSQL.setBounds(2*D+T, N+2*D, B, N);
        textSql.setLineWrap(true);
        textPreparedSql.setLineWrap(true);
        textSql.setBounds(D, N+2*D, T, E);
        textPreparedSql.setBounds(D*3 +T + B, N+2*D, T, E);
        textSql.setAutoscrolls(true);
        textPreparedSql.setAutoscrolls(true);
        sql2preparedSQL.addActionListener(new SqlToPreparedSQLActionListener());
        generateMap.addActionListener(new GenerateActionListener());
        useDefaultMap.addActionListener(new DefaultActionListener());
        frame.add(sql2preparedSQL);
        frame.add(useDefaultMap);
        frame.add(generateMap);
        frame.add(baseMap);
        textSql.setBounds(D, N+2*D, T, E);
        textPreparedSql.setBounds(D*3 +T + B, N+2*D, T, E);
        frame.add(textPreparedSql);
        frame.add(textSql);
    }

    private String generateMap() {
        TreeMap<Double, Character> randomChars = new TreeMap<>();
        for (char ch : BAD_CHARACTERS.toCharArray()) {
            Double random = Math.random();
            while (randomChars.containsKey(random)) {
                random = Math.random();
            }
            randomChars.put(random, ch);
        }
        StringBuilder result = new StringBuilder();
        for (char ch : randomChars.values()) {
            result.append(ch);
        }
        return new String(result);
    }

    private class SqlToPreparedSQLActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String sqlString = textSql.getText();
            StringBuilder preparedSqlString = new StringBuilder();
            for (char chr : sqlString.toCharArray()) {
                String current = String.valueOf(chr);
                if (baseMap.getText().contains(String.valueOf(chr))) {
                    current = "'||CHR(" + String.valueOf((int) chr) + ")||'";
                }
                preparedSqlString.append(current);
            }
            textPreparedSql.setText(preparedSqlString.toString());
        }
    }
    private class GenerateActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            baseMap.setText(generateMap());
        }
    }
    private class DefaultActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            baseMap.setText(BAD_CHARACTERS);
        }
    }

}
