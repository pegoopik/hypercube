package ui.examples.hypercube.ui;

import javax.swing.*;
import java.awt.*;

public class CubeNDFrame extends JFrame {

    public CubeNDFrame(CubeNDPanel cubeNDPanel, int sizeX, int sizeY) {
        setSize(sizeX, sizeY);
        getContentPane().add(cubeNDPanel);
    }

}
